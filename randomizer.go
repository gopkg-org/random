/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package random

import (
	"math/rand"
	"time"
	"unsafe"
)

const (
	LatinLower  Standard = `abcdefghijklmnopqrstuvwxyz`
	LatinUpper  Standard = `ABCDEFGHIJKLMNOPQRSTUVWXYZ`
	Decimal     Standard = `0123456789`
	Hexadecimal Standard = `0123456789ABCDEF`
	Symbols     Standard = `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`

	Latin        = LatinLower + LatinUpper
	Alphanumeric = Latin + Decimal
	Full         = Alphanumeric + Symbols
)

type Randomizer interface {
	Generate(length int) string
}

type RandomizerFunc func(length int) string

func (f RandomizerFunc) Generate(length int) string { return f(length) }

const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

type Standard string

func (r Standard) Generate(length int) string {
	src := rand.NewSource(time.Now().UnixNano())

	value := make([]byte, length)

	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for increment, cache, remain := length-1, src.Int63(), letterIdxMax; increment >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}

		if idx := int(cache & letterIdxMask); idx < len(r) {
			value[increment] = r[idx]
			increment--
		}

		cache >>= letterIdxBits

		remain--
	}

	return *(*string)(unsafe.Pointer(&value))
}
