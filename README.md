# Random

Makes random string with multi randomizers.

## Install

```shell
go get gopkg.org/random
```

## Example

### Simple

```go
package main

import (
    "fmt"
    "gopkg.org/random"
)

func main() {
    fmt.Println(random.New(8))
}
```

### With custom Randomizer

```go
package main

import (
    "fmt"
    "gopkg.org/random"
)

func main() {
    fmt.Println(random.NewWithRandomizer(RepeatRandomizer(), 64))
    // Output: test-test-test-test-test-test-test-test-test-test-test-test-test
}

func RepeatRandomizer() random.Randomizer {
    return random.RandomizerFunc(func(length int) string {
        str, res := "test-", ""
        for i := 0; i < (length / len(str)); i++ {
            res += str
        }
        
        if m := length % 5; m > 0 && m <= len(str) {
            res += str[:m]
        }
        
        return res
	})
}
```