/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package random_test

import (
	"fmt"
	"gopkg.org/random"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func ExampleNew() {
	fmt.Println(random.New(16))
}

func TestNew(t *testing.T) {
	suite, value := assert.New(t), random.New(64)
	suite.Equal(64, len(value))
	suite.True(strings.ContainsAny(value, `abcdefghijklmnopqrstuvwxyz`))
	suite.True(strings.ContainsAny(value, `ABCDEFGHIJKLMNOPQRSTUVWXYZ`))
	suite.True(strings.ContainsAny(value, `0123456789`))
	suite.False(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`+"`"))
}

func ExampleNewWithRandomizer() {
	repeatRandomizer := random.RandomizerFunc(func(length int) string {
		str, res := "test-", ""
		for i := 0; i < (length / len(str)); i++ {
			res += str
		}

		if m := length % 5; m > 0 && m <= len(str) {
			res += str[:m]
		}

		return res
	})

	fmt.Println(random.NewWithRandomizer(repeatRandomizer, 64))
	// Output: test-test-test-test-test-test-test-test-test-test-test-test-test
}

func TestNewWithRandomizer(t *testing.T) {
	repeatRandomizer := random.RandomizerFunc(func(length int) string {
		str, res := "test-", ""
		for i := 0; i < (length / len(str)); i++ {
			res += str
		}

		if m := length % 5; m > 0 && m <= len(str) {
			res += str[:m]
		}

		return res
	})

	value := random.NewWithRandomizer(repeatRandomizer, 64)
	assert.New(t).Equal("test-test-test-test-test-test-test-test-test-test-test-test-test", value)
}
