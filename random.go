/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package random

func New(length int) string {
	return NewWithRandomizer(Alphanumeric, length)
}

func NewWithRandomizer(randomizer Randomizer, length int) string {
	return randomizer.Generate(length)
}
