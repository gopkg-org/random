/*
 * Copyright (c) 2023, Thomas FOURNIER and contributors. All rights reserved.
 * Licensed under the MIT license. See LICENSE file in the project root for details.
 */

package random_test

import (
	"gopkg.org/random"
	"strings"
	"testing"

	"github.com/stretchr/testify/suite"
)

func TestRandomizer(t *testing.T) {
	t.Parallel()

	suite.Run(t, new(RandomizerSuite))
}

type RandomizerSuite struct {
	suite.Suite
}

func (suite *RandomizerSuite) TestLatinLower() {
	value := random.LatinLower.Generate(64)
	suite.Equal(64, len(value))
	suite.True(strings.ContainsAny(value, `abcdefghijklmnopqrstuvwxyz`))
	suite.False(strings.ContainsAny(value, `ABCDEFGHIJKLMNOPQRSTUVWXYZ`))
	suite.False(strings.ContainsAny(value, `0123456789`))
	suite.False(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`))
}

func (suite *RandomizerSuite) TestLatinUpper() {
	value := random.LatinUpper.Generate(64)
	suite.Equal(64, len(value))
	suite.False(strings.ContainsAny(value, `abcdefghijklmnopqrstuvwxyz`))
	suite.True(strings.ContainsAny(value, `ABCDEFGHIJKLMNOPQRSTUVWXYZ`))
	suite.False(strings.ContainsAny(value, `0123456789`))
	suite.False(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`))
}

func (suite *RandomizerSuite) TestLatin() {
	value := random.Latin.Generate(64)
	suite.Equal(64, len(value))
	suite.True(strings.ContainsAny(value, `abcdefghijklmnopqrstuvwxyz`))
	suite.True(strings.ContainsAny(value, `ABCDEFGHIJKLMNOPQRSTUVWXYZ`))
	suite.False(strings.ContainsAny(value, `0123456789`))
	suite.False(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`))
}

func (suite *RandomizerSuite) TestDecimal() {
	value := random.Decimal.Generate(64)
	suite.Equal(64, len(value))
	suite.False(strings.ContainsAny(value, `abcdefghijklmnopqrstuvwxyz`))
	suite.False(strings.ContainsAny(value, `ABCDEFGHIJKLMNOPQRSTUVWXYZ`))
	suite.True(strings.ContainsAny(value, `0123456789`))
	suite.False(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`))
}

func (suite *RandomizerSuite) TestHexadecimal() {
	value := random.Hexadecimal.Generate(64)
	suite.Equal(64, len(value))
	suite.True(strings.ContainsAny(value, `0123456789ABCDEF`))
	suite.False(strings.ContainsAny(value, "GHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"))
	suite.False(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`))
}

func (suite *RandomizerSuite) TestSymbols() {
	value := random.Symbols.Generate(64)
	suite.Equal(64, len(value))
	suite.False(strings.ContainsAny(value, `abcdefghijklmnopqrstuvwxyz`))
	suite.False(strings.ContainsAny(value, `ABCDEFGHIJKLMNOPQRSTUVWXYZ`))
	suite.False(strings.ContainsAny(value, `0123456789`))
	suite.True(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`))
}

func (suite *RandomizerSuite) TestAlphanumeric() {
	value := random.Alphanumeric.Generate(64)
	suite.Equal(64, len(value))
	suite.True(strings.ContainsAny(value, `abcdefghijklmnopqrstuvwxyz`))
	suite.True(strings.ContainsAny(value, `ABCDEFGHIJKLMNOPQRSTUVWXYZ`))
	suite.True(strings.ContainsAny(value, `0123456789`))
	suite.False(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`))
}

func (suite *RandomizerSuite) TestFull() {
	value := random.Full.Generate(64)
	suite.Equal(64, len(value))
	suite.True(strings.ContainsAny(value, `abcdefghijklmnopqrstuvwxyz`))
	suite.True(strings.ContainsAny(value, `ABCDEFGHIJKLMNOPQRSTUVWXYZ`))
	suite.True(strings.ContainsAny(value, `0123456789`))
	suite.True(strings.ContainsAny(value, `!"#$%&'()*+,-./:;<=>?@[\]^_{|}~`))
}

func (suite *RandomizerSuite) TestCustom() {
	value := random.Standard("123abc").Generate(64)
	suite.Equal(64, len(value))
	suite.True(strings.ContainsAny(value, "123abc"))
	suite.False(strings.ContainsAny(value, "456789defghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"))

}
